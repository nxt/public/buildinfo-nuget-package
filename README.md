# DynamicBuildInfo

Simple nuget package that generates a partial `ThisAssembly` class with build information.

All fields inside the `ThisAssembly` class are compile-time constants,
meaning they can be used in a project's `AssemblyInfo.cs` file.

This allows using dynamic* information in the `AssemblyInfo.cs` file,
such as the current year at build time.
(I am sure it comes at no surprise that I created this package in early January)

(*dynamic in the sense that it is not hard-coded but calculated at build time)

## Usage

Install the nuget package in your project.

All the generated fields are added to a partial class called `ThisAssembly`.

Use it in `AssemblyInfo.cs`  like this:

```csharp
public const string Copyright = $"(c) {ThisAssembly.BuildInfo.Properties.Year} YourCompany";
```

Or as an attribute:

```csharp
[assembly: Copyright($"(c) {ThisAssembly.BuildInfo.Properties.Year} YourCompany")]
```

## Available fields

The following fields are available:

- `Project.Properties.Year`- the current year at build time

## Add fields

You can add additional fields to the `ThisAssembly.BuildInfo.Properties`
class by specifying them in the `.csproject` file.

For example:

```xml
<ItemGroup>
    <DynamicBuildInfoProperties Include="SomeGuid">
        <Summary>Some GUID</Summary>
        <Value>$([System.Guid]::NewGuid())</Value>
    </DynamicBuildInfoProperties>
</ItemGroup>
```

This will add a field called `SomeGuid` to the `ThisAssembly.BuildInfo.Properties` class.

Each field you want to add must be specified  in a separate `BuildInfoProperties` item.

The `Include` attribute specifies the name of the field and the `Summary` attribute is added as a comment to the field.

The `Value` attribute specifies the value of the field.
It can be a static value or a MSBuild expression; see also [Property Functions](https://learn.microsoft.com/en-us/visualstudio/msbuild/property-functions)

## Further reading

This package is heavily inspired by the [GitInfo](https://github.com/devlooped/GitInfo)
and [MSBuilder](https://github.com/MobileEssentials/MSBuilder) packages.
